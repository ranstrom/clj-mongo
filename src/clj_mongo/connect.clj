(ns clj-mongo.connect
  (:require [taoensso.timbre :as log])
  (:import (com.mongodb MongoClient MongoClientURI)))

(defprotocol Connection
  (close [this]))

(defrecord MongoDBConnection [mongo db]
  Connection
  (close [_]
    (log/debug "Closing connection.")
    (.close ^MongoClient mongo)))

(defn- ->mongo-uri ^MongoClientURI [^String uri]
  (MongoClientURI. uri))

(defn- ->mongo-client ^MongoClient [^MongoClientURI client-uri]
  (MongoClient. client-uri))

(defn make-connection
  ([^String uri]
   (make-connection uri nil))
  ([^String uri ^String db]
   (let [client (-> uri ->mongo-uri ->mongo-client)
         conn-db (or db (.getDatabase (->mongo-uri uri)))]
     (MongoDBConnection. client (.getDB client conn-db)))))