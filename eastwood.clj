(disable-warning
 {:linter :deprecations
  :symbol-matches #{#"^public com\.mongodb\.DB com\.mongodb\.Mongo\.getDB\(java\.lang\.String\)$"}})

(disable-warning
 {:linter :wrong-arity
  :function-symbol 'somnium.congomongo/update!
  :arglists-for-linting '([coll old new & {:keys [upsert multiple as from write-concern]
                                           :or {upsert true multiple false as :clojure from :clojure}}])
  :reason "somnium.congomongo/update! uses metadata to override the default value of :arglists for documentation purposes."})

(disable-warning
 {:linter :wrong-arity
  :function-symbol 'somnium.congomongo/fetch
  :arglists-for-linting '([coll & {:as params}])
  :reason "somnium.congomongo/fetch uses metadata to override the default value of :arglists for documentation purposes."})

(disable-warning
 {:linter :wrong-arity
  :function-symbol 'somnium.congomongo/aggregate
  :arglists-for-linting '([coll op & ops-and-from-to])
  :reason "somnium.congomongo/aggregate uses metadata to override the default value of :arglists for documentation purposes."})

(disable-warning
 {:linter :wrong-arity
  :function-symbol 'somnium.congomongo/destroy!
  :arglists-for-linting '([c q & {:keys [from write-concern]
                                  :or {from :clojure}}])
  :reason "somnium.congomongo/destroy uses metadata to override the default value of :arglists for documentation purposes."})

(disable-warning
 {:linter :wrong-arity
  :function-symbol 'somnium.congomongo/mass-insert!
  :arglists-for-linting '([coll objs & {:keys [from to write-concern]
                                        :or {from :clojure to :clojure}}])
  :reason "somnium.congomongo/mass-insert! uses metadata to override the default value of :arglists for documentation purposes."})